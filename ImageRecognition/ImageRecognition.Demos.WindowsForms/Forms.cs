﻿using Emgu.CV;
using Emgu.CV.Cuda;
using Emgu.CV.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageRecognition.Demos.WindowsForms
{
    public partial class Forms : Form
    {
        public Forms()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();     //显示选择文件对话框
            openFileDialog1.Filter = "All files (*.*)|*.*|image files |*.jpg;*.jpeg;*.png;*.gif;*.bmp";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                this.orgimgpath.Text = openFileDialog1.FileName;          //显示文件路径
                ShowOrgImage();
            }
            openFileDialog1.Dispose();
        }
        private void ShowOrgImage()
        {
            orgimg.Refresh();
            using (var img = Image.FromFile(this.orgimgpath.Text))
            {
                orgimg.Image = (Image)img.Clone();
            }
        }

        private void facedetection_btn_Click(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                string imagePath = orgimgpath.Text;

                long detectionTime;

                IImage image = FaceDetection.Tagging(imagePath, "data/haarcascade_frontalface_default.xml", "data/haarcascade_eye.xml", out detectionTime);
                //display the image 
                using (InputArray iaImage = image.GetInputArray())
                    ImageViewer.Show(image, String.Format(
                       "检测使用  {0} 模式，耗时 {1} milliseconds",
                       (iaImage.Kind == InputArray.Type.CudaGpuMat && CudaInvoke.HasCuda) ? "CUDA" :
                       (iaImage.IsUMat && CvInvoke.UseOpenCL) ? "OpenCL"
                       : "CPU",
                       detectionTime));
            });
        }

        private void pedestriandetectionbtn_Click(object sender, EventArgs e)
        {
            string imagePath = orgimgpath.Text;

            long detectionTime;
            Task.Run(() =>
            {
                IImage image = PedestrianDetection.Tagging(imagePath, out detectionTime);
                //display the image 
                ImageViewer.Show(
                   image,
                   String.Format("Pedestrian detection using {0} in {1} milliseconds.",
                      CudaInvoke.HasCuda ? "GPU" :
                      CvInvoke.UseOpenCL ? "OpenCL" :
                      "CPU",
                      detectionTime));
            });
        }
    }
}
